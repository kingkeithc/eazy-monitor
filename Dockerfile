FROM python:3.7

LABEL maintainer="Keith King <KingKeithC@gmail.com>"

ARG EZ_VERSION="Undefined"

ENV EZ_ENVIRONMENT="DefaultProduction"
ENV EZ_HOST="0.0.0.0"
ENV EZ_PORT="8080"
ENV EZ_UID="1000"
ENV EZ_GID="1000"
ENV EZ_APP_DIR="/eazy-monitor"

RUN if ! getent group ${EZ_GID}; then groupadd -g ${EZ_GID} eazy-monitor; fi && \
    if ! getent passwd ${EZ_UID}; then useradd  -u ${EZ_UID} eazy-monitor -g ${EZ_GID}; fi && \
    echo -e "UID: ${EZ_UID}\nGID: ${EZ_GID}" && id ${EZ_UID} && id ${EZ_GID} && \
    mkdir ${EZ_APP_DIR} && chown -R ${EZ_UID}:${EZ_GID} ${EZ_APP_DIR}

WORKDIR ${EZ_APP_DIR}
COPY --chown=eazy-monitor:eazy-monitor . .

RUN pip install -r requirements.txt

EXPOSE ${EZ_PORT}

USER ${EZ_UID}:${EZ_GID}

CMD flask run --no-debugger --no-reload --host ${EZ_HOST} --port ${EZ_PORT}
