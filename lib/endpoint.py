"""Defines the structure of an endpoint object."""
import copy
import logging
import datetime
import requests


class Endpoint():
    """Describes the endpoint object."""

    def __init__(self, authority, basic_auth=None):
        """Initialize the Endpoint.

        authority: The scheme://host[:port] part of this site's URL.
        basic_auth: If the endpoint requires HTTP basic authentication.
            Specify a tuple of (username, password) to this parameter.
        """
        self.authority = authority
        self._log = logging.getLogger('app.endpoint.Endpoint')
        self._test_results = []
        self._tests = []

        # Create a session, and add our auth to it
        self._session = requests.Session()
        self._session.auth = basic_auth

    def test(self):
        """Run all tests in this site's list of tests."""
        for test in self._tests:
            # Make a full copy of the test
            # we do not want to modify the stored test
            test_to_run = copy.deepcopy(test)

            # Make the actual request, handle some common errors
            exception = None
            response = None
            test_time = datetime.datetime.now()
            try:
                response = self._session.send(test_to_run['req'])
            except requests.ConnectTimeout as e:
                # If there is a connection error
                self._log.error('Timeout Making %s request to %s.',
                                test_to_run.method,
                                test_to_run.url)
                exception = e
            except requests.ConnectionError as e:
                # If the connection times out
                self._log.error('Connection Error Making %s request to %s.',
                                test_to_run.method,
                                test_to_run.url)
                exception = e

            result = {
                'time_of_test': test_time,
                'test_name': test_to_run['name'],
                'request': test_to_run['req'],
                'response': response,
                'exception': exception
            }

            self._test_results.append(result)

    def add_test(self, name, method='GET', route='/', timeout=5):
        """Add a test to this site's list of tests."""
        url = self.authority + route
        test_request = requests.Request(method=method, url=url)
        prepared_request = self._session.prepare_request(test_request)

        # Append the test to this site's list of tests
        self._tests.append({'name': name, 'req': prepared_request})
