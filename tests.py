import unittest
from app import APP

class BasicTestCase(unittest.TestCase):

    def test_root_route(self):
        tester = APP.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
