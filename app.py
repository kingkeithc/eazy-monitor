from flask import Flask, render_template
import os
import logging
import requests
import datetime

from lib import endpoint

# Set the log line format
log_format = logging.Formatter(
    '%(asctime)s - %(levelname)s (%(name)s): %(message)s')

# Create a handler for logging to a file, and set its format
file_handler = logging.FileHandler('eazy-monitor.log', encoding='UTF-8')
file_handler.setFormatter(log_format)

# Create a handler for logging to the console, and set its format
console_handler = logging.StreamHandler()
console_handler.setFormatter(log_format)

# Get the logger that we will be using here
log = logging.getLogger('app')

# Add the file and console handlers
log.addHandler(console_handler)
log.addHandler(file_handler)

# Set the log level
log.setLevel(logging.DEBUG)

# Print a message to let everyone know its working
log.debug('Logging configured for module %s.', __name__)


VERSION = os.environ.get('EZ_VERSION', default='Not Specified.')
log.info(f"Running on Version: ${VERSION}")


APP = Flask(__name__)


def test_website(url):
    """Return details about whether the `url` is responding."""
    log.info('Getting details about %s', url)
    test_time = datetime.datetime.now()

    try:
        response = requests.get(url)
    except requests.exceptions.ConnectionError:
        log.info('Connection Error for site %s', url)
        details = {
            'url': url,
            'test_type': 'GET',
            'is_ok': False,
            'status_code': None,
            'time_of_test': test_time.strftime('%Y-%m-%d %H:%M:%S')
        }
        pass
    else:
        details = {
            'url': url,
            'test_type': 'GET',
            'is_ok': response.ok,
            'status_code': response.status_code,
            'time_of_test': test_time.strftime('%Y-%m-%d %H:%M:%S')
        }

    return details


@APP.route('/')
def root_route():
    return render_template('root.html', VERSION=VERSION)


@APP.route('/status')
def status_route():
    google = endpoint.Endpoint('https://google.ca')
    google.add_test('Search Page')
    google.test()
    result = google._test_results[-1]
    return render_template('status.html', page_title='Status', statuses=[result], VERSION=VERSION)
